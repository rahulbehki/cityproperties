<?php

namespace App\Http\Controllers\UserManagement;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserMgmtController extends Controller
{
    public function _construct()
    {
        $this->middleware('admin');
    }
    public function AddUser()
    {
        return view('UserManagement.adduser');
        //return view('admin.layout');
    }
}
