<?php

//Route::get('/', function () {
//    return view('login');
//});

Route::get('/', 'Adminauth\AuthController@showLoginForm');
Route::post('login', 'Adminauth\AuthController@login');

Route::group(['middleware' => ['admin']], function () {
    Route::get('/dashboard', 'Admin\AdminController@dashboard');
    Route::get('/logout', 'Adminauth\AuthController@logout');
});

Route::get('UserManage/Add', 'UserManagement\UserMgmtController@AddUser');

//Route::get('/create', function () {
//    App\User::create([
//        'name' => 'rahul',
//        'username' => 'rahul',
//        'email' => 'rahul.1989.22@gmail.com',
//        'password' => bcrypt('password')
//    ]);
//});

//Route::get('/dashboard', function () {
//    return view('admin.layout');
//});
