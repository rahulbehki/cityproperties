@extends('admin.layout')
@section('content')
<div class="main-content" >
    <div class="wrap-content container" id="container">
        <section id="page-title">
            <div class="row">
                <div class="col-sm-8">
                    <h1 class="mainTitle">User Management</h1>
<!--									<span class="mainDescription">
                        We set out to create an easy, powerful and versatile form layout system. 
                        A combination of form styles and the Bootstrap grid means you can do 
                        almost anything.
                    </span>-->
                </div>
                <ol class="breadcrumb">
                    <li>
                        <span>User Management</span>
                    </li>
                    <li class="active">
                        <span>Add User</span>
                    </li>
                </ol>
            </div>
        </section>
        <div class="container-fluid container-fullw bg-white">
            <div class="row">
                <div class="col-md-12">
                    <div class="row margin-top-30">
                        <div class="col-lg-6 col-md-12">
                            <div class="panel panel-white">
                                <div class="panel-heading">
                                    <h5 class="panel-title">Add User</h5>
                                </div>
                                <div class="panel-body">

                                    <form role="form" data-parsley-validate="">
                                        <div class="form-group">
                                            <label>
                                                Full Name <span class="symbol required"></span>
                                            </label>
                                            <input type="text" name="fullname" required class="form-control" placeholder="Enter Name">
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                Username <span class="symbol required"></span>
                                            </label>
                                            <input type="text" name="username" required class="form-control" placeholder="Enter Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">
                                                Email address <span class="symbol required"></span>
                                            </label>
                                            <input type="email" name="email" required class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                                        </div>
                                        <div class="form-group">
                                            <label for="form-field-mask-2">
                                                Contact No. 
                                            </label>
                                            <div class="input-group">
                                                <span class="input-group-addon"> <i class="fa fa-phone"></i> </span>
                                                <input type="text" required id="form-field-mask-2" class="form-control input-mask-phone">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">
                                                Password <span class="symbol required"></span>
                                            </label>
                                            <input type="password" name="password" required class="form-control" id="exampleInputPassword1" placeholder="Password">
                                        </div>
                                        <div class="form-group">
                                            <label class="block">
                                                Gender
                                            </label>
                                            <div class="clip-radio radio-primary">
                                                <input type="radio" id="female" name="gender" value="female">
                                                <label for="female">
                                                    Female
                                                </label>
                                                <input type="radio" id="male" name="gender" value="male" checked="checked">
                                                <label for="male">
                                                    Male
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">
                                                Upload <span class="symbol required"></span>
                                            </label>
                                            <input type="file" name="upload" required class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                                        </div>
                                        <button type="submit" class="btn btn-o btn-primary">
                                            Submit
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="panel panel-white">
                                <div class="panel-heading">
                                    <h5 class="panel-title">Users Info</h5>
                                </div>
                                <div class="panel-body">

                                    <div class="table-responsive">
                                        <table class="table table-condensed" id="sample-table-1">
                                            <thead>
                                                <tr>
                                                    <th style="font-size: 13px;text-align: center;">Photo</th>
                                                    <th style="font-size: 13px;text-align: center;">Name</th>
                                                    <th style="font-size: 13px;text-align: center;">Status</th>
                                                    <th style="font-size: 13px;text-align: center;">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td style="text-align: center;">
                                                        <a href="#">
                                                            <img src="assets/images/avatar-1.jpg" class="img-rounded" alt="image"/>
                                                        </a></td>
                                                    <td  style="text-align: center;font-size: 13px;">Peter Clark</td>
                                                    <td  style="text-align: center;font-size: 13px;"><span class="label label-sm label-success">Active</span></td>
                                                    <td class="center">
                                                        <a href="#" class="btn btn-transparent btn-xs" tooltip-placement="top" tooltip="Edit"><i class="fa fa-pencil"></i></a>
                                                        <a href="#" class="btn btn-transparent btn-xs tooltips" tooltip-placement="top" tooltip="Share"><i class="fa fa-share"></i></a>

                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="text-align: center;">
                                                        <a href="#">
                                                            <img src="assets/images/avatar-2.jpg" class="img-rounded" alt="image"/>
                                                        </a></td>
                                                    <td  style="text-align: center;font-size: 13px;">Nicole Bell</td>
                                                    <td  style="text-align: center;font-size: 13px;"><span class="label label-sm label-success">Active</span></td>
                                                    <td class="center">
                                                        <a href="#" class="btn btn-transparent btn-xs" tooltip-placement="top" tooltip="Edit"><i class="fa fa-pencil"></i></a>
                                                        <a href="#" class="btn btn-transparent btn-xs tooltips" tooltip-placement="top" tooltip="Share"><i class="fa fa-share"></i></a>

                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="text-align: center;">
                                                        <a href="#">
                                                            <img src="assets/images/avatar-3.jpg" class="img-rounded" alt="image"/>
                                                        </a></td>
                                                    <td  style="text-align: center;font-size: 13px;">Steven Thompson</td>
                                                    <td  style="text-align: center;font-size: 13px;"><span class="label label-sm label-success">Active</span></td>
                                                    <td class="center">
                                                        <a href="#" class="btn btn-transparent btn-xs" tooltip-placement="top" tooltip="Edit"><i class="fa fa-pencil"></i></a>
                                                        <a href="#" class="btn btn-transparent btn-xs tooltips" tooltip-placement="top" tooltip="Share"><i class="fa fa-share"></i></a>

                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="text-align: center;">
                                                        <a href="#">
                                                            <img src="assets/images/avatar-4.jpg" class="img-rounded" alt="image"/>
                                                        </a></td>
                                                    <td  style="text-align: center;font-size: 13px;">Ella Patterson</td>
                                                    <td  style="text-align: center;font-size: 13px;"><span class="label label-sm label-success">Active</span></td>
                                                    <td class="center">
                                                        <a href="#" class="btn btn-transparent btn-xs" tooltip-placement="top" tooltip="Edit"><i class="fa fa-pencil"></i></a>
                                                        <a href="#" class="btn btn-transparent btn-xs tooltips" tooltip-placement="top" tooltip="Share"><i class="fa fa-share"></i></a>

                                                    </td>

                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
