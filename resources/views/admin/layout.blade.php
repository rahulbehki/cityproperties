<html lang="en">
    <head>
        <title>Admin Panel | City Properties </title>
        <base href="http://localhost/citypropertieslaravel/"/>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta content="" name="description" />
        <meta content="" name="author" />
        <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
        
        {{ Html::style('vendor/bootstrap/css/bootstrap.min.css') }}
        
        {{ Html::style('vendor/fontawesome/css/font-awesome.min.css') }}
        
        {{ Html::style('vendor/themify-icons/themify-icons.min.css') }}
        
        {{ Html::style('vendor/animate.css/animate.min.css') }}
        
        {{ Html::style('vendor/perfect-scrollbar/perfect-scrollbar.min.css') }}
        
        {{ Html::style('vendor/switchery/switchery.min.css') }}
        
        {{ Html::style('assets/css/styles.css') }}
        
        {{ Html::style('assets/css/plugins.css') }}
        
        {{ Html::style('assets/css/themes/theme-1.css') }}
        
        {{ Html::style('assets/css/parsley.css') }}
    </head>
    <body>
        <div id="app">
            @if (Auth::guard('admin')->check())
                @include('admin.header.header')
                @include('admin.sidebar.sidebar')
                @yield('content')
            @else
                @yield('content')
            @endif
        </div>
    </body>
    
    {{ Html::script('vendor/jquery/jquery.min.js') }}
    
    {{ Html::script('vendor/bootstrap/js/bootstrap.min.js') }}
    
    {{ Html::script('vendor/modernizr/modernizr.js') }}
    
    {{ Html::script('vendor/jquery-cookie/jquery.cookie.js') }}
    
    {{ Html::script('vendor/perfect-scrollbar/perfect-scrollbar.min.js') }}
    
    {{ Html::script('vendor/switchery/switchery.min.js') }}
    
    {{ Html::script('vendor/Chart.js/Chart.min.js') }}
    
    {{ Html::script('vendor/jquery.sparkline/jquery.sparkline.min.js') }}
    
    {{ Html::script('vendor/maskedinput/jquery.maskedinput.min.js') }}
    
    {{ Html::script('assets/js/main.js') }}
    
    {{ Html::script('assets/js/index.js') }}
    
    {{ Html::script('assets/js/parsley.min.js') }}
    
    
    <script>
        jQuery(document).ready(function () {
            Main.init();
            Index.init();
        });
    </script>
</html>